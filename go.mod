module gitee.com/cowpeatechnology/pb

go 1.19

require google.golang.org/protobuf v1.28.1

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/infobloxopen/protoc-gen-gorm v1.1.2 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.3.0 // indirect
)
