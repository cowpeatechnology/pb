# Changelog
##[0.6.42] - 2022-12-18
### Added
- 添加 AddPlayerMoneyReq 消息


##[0.6.41] - 2022-12-18
### Changed
- 修改 ClaimResourcesRes 消息

##[0.6.39] - 2022-12-16
### Changed
- 修改  BuildingHouseMsg 消息
- 修改 SmeltingCoolTimeNotify 消息



##[0.6.37] - 2022-12-16
### Changed
- 修改 FusionPersonReq 消息



##[0.6.36] - 2022-12-16
### Changed
- 修改 Recipe 结构


##[0.6.35] - 2022-12-16
### Changed
- 修改 DeleteBuildRes 消息
- 修改 UpgradingBuildingsRes 消息

##[0.6.33] - 2022-12-15
### Changed
- 修改 GetResourcesReq 消息


##[0.6.30] - 2022-12-15
### Changed
- 修改 PersonBattleReq 消息

##[0.6.28] - 2022-12-14
### Added
- 修改 PlunderReq 消息


##[0.6.26] - 2022-12-13
### Changed
- 修改 Recipe 结构
- 修改 FamilyHouse 结构



##[0.6.25] - 2022-12-13
### Changed
- 修改 PersonSettleInRes 消息



##[0.6.24] - 2022-12-13
### Changed
- 修改 PersonSettleInReq 消息

##[0.6.22] - 2022-12-13
### Changed
- 修改 DeleteBuildRes 消息
- 修改 DeleteBuildReq 消息


##[0.6.21] - 2022-12-12
### Changed
- 修改 ProduceEquipmentReq 消息
- 修改 EquipmentInfoExtend 结构
- 修改 FamilyHouseInfo 结构

##[0.6.19] - 2022-12-7
### Changed
- 修改 ConstructionHomeBuildingsRes 消息



##[0.6.19] - 2022-12-7
### Changed
- 添加 GetMyGuildRes 消息

##[0.6.14] - 2022-12-6
### Added
- 添加 GetsMyWarehouseReq 消息



##[0.6.13] - 2022-12-6
### Added
- 添加 GetWareHouseUpgradeInfoReq 消息

##[0.6.12] - 2022-12-5
### Added
- 添加 SearchForFamilyReq 消息
- 添加 PersonBattleReq 消息
- 添加 PersonSettleInReq 消息

##[0.6.5] - 2022-12-4
### Changed
- 修改 PurchaseOrderList    =>   GetOrders
- 修改 PlayerProcure  => PostSaleOrder
- 修改 SubmitToOrders  => FulfillOrder
- 修改 SendsOutNotify  =>  TransportProduct


##[0.6.4] - 2022-12-4
### Changed
- 修改 SubmitToOrdersReq 消息
- 

##[0.6.3] - 2022-12-3
### Added
- 添加 trade.proto 模块



##[0.6.0] - 2022-12-2
### Added
- 添加 GetGameVersionReq 消息
- 修改 ZiJieLoginReq 消息


##[0.5.99] - 2022-12-1
### Added
- 添加 ShareRewardsReq 消息

##[0.5.98] - 2022-11-28
### Added
- 添加 GetClaimStatusReq 消息



##[0.5.97] - 2022-11-28
### Added
- 添加 ClaimSidebarRewardsReq 消息

##[0.5.96] - 2022-11-27
### Added
- 添加 RedeemRewardsReq 消息

##[0.5.91] - 2022-11-15
### Changed
- 修改 PingReq 消息

##[0.5.90] - 2022-11-13
### Changed
- 修改 BasePlayer 结构

##[0.5.87] - 2022-11-3
### Added 
- 添加 GetsPersonLevelReq 消息
- 添加 PersonLevel 结构


##[0.5.86] - 2022-11-3
### Changed
- 修改 ZiJieLoginRes 消息



##[0.5.85] - 2022-11-2
### Changed
- 修改 TaskReward 结构
- 修改  PingRes 消息

##[0.5.84] - 2022-11-2
### Changed
- 修改 PingReq 消息



##[0.5.83] - 2022-10-24
### Changed
- 修改 ZiJieLoginReq 消息

##[0.5.82] - 2022-10-24
### Changed
- 修改 ReadMailRes 消息


##[0.5.80] - 2022-10-24
### Changed
- 修改 GetPersonRes 消息
- 修改 SpeciesRanking 结构

##[0.5.79] - 2022-10-24
### Changed
- 修改 GetSignInTableRes 消息


##[0.5.78] - 2022-10-20
### Changed
- 修改 TreasureChest 结构
- 修改 GetPersonExpLevelRankingRes 消息


##[0.5.74] - 2022-10-19
### Added
- 添加 ClaimTreasureChestReq 消息


##[0.5.73] - 2022-10-19
### Added
- 添加 PersonTravelReq 消息



##[0.5.71] - 2022-10-18
### Changed
- 修改 ClaimRewardsReq 消息

##[0.5.68] - 2022-10-17
### Added
- 添加 GetSpeciesRankingReq 消息
- 添加 GetGuildMapPosRankingReq 消息
- 添加 GetPersonExpLevelRankingReq 消息
- 添加 GetSignInTableReq 消息
- 添加 SignInReq 消息



##[0.5.64] - 2022-10-16
### Added
- 添加 GetWorldTaskReq 消息
- 添加 ClaimRewardsReq 消息



##[0.5.62] - 2022-10-11
### Changed
- 修改 Code  ReplaceAdventureTeam 消息

##[0.5.57] - 2022-9-27
### Added
- 添加 GetsTroopConfigurationTableReq 消息

##[0.5.56] - 2022-9-20
### Changed
- 修改 MonsterTeam 结构

##[0.5.55] - 2022-9-20
### Changed
- 修改 GetsArmyAttributeRes 消息

##[0.5.54] - 2022-9-20
### Changed
- 修改 ArmyAttribute 结构

##[0.5.53] - 2022-9-20
### Changed
- 修改 GetAdventureTeamRes  消息
- 修改 CreateAdventureTeamReq 消息
- 修改 RecruitSoldiersMaxReq 消息

##[0.5.52] - 2022-9-20
### Changed
- 修改 Guild 结构

##[0.5.50] - 2022-9-20
### Changed
- 修改 BasePlayer 结构
- 修改 UseArmyAttributeBookRes 消息

##[0.5.49] - 2022-9-19
### Changed
- 修改 UseArmyAttributeBookReq 消息

##[0.5.46] - 2022-9-18
### Add
- 添加 ArmyAttributeTransferReq 消息
- 修改  Army 消息
- 修改 ArmyDetails 消息

##[0.5.44] - 2022-9-18
### Changed
- 修改 CreateArmyReq 消息
- 添加 GetsArmyAttributeReq 消息
- 添加 UseArmyAttributeBookReq 消息
- 添加 ResettingArmyAttributeReq 消息

##[0.5.41] - 2022-9-13
### Changed
- 修改 VisibleArmyNotify 消息

##[0.5.40] - 2022-9-11
### Changed
- 修改 PersonProperty 结构

##[0.5.38] - 2022-9-7
### Changed
- 修改 UsedEquipmentGoodReq 消息

##[0.5.37] - 2022-9-4
### Changed
- 修改 TrainingGroundTask 消息

##[0.5.35] - 2022-9-2
### Changed
- 修改 RecycleReq 消息

##[0.5.34] - 2022-8-31
### Changed
- 修改  SoldiersCount 结构

##[0.5.31] - 2022-8-26
### Changed
- 修改 Code 消息

##[0.5.30] - 2022-8-25
### Added
- 添加 GetsEquipmentInfoReq 消息

##[0.5.29] - 2022-8-24
### Changed
- 添加 ExitStatus 消息
- 添加 SubSkillStatus 消息


##[0.5.28] - 2022-8-18
### Changed
- 修改 SkillEffects

##[0.5.27] - 2022-8-15
### Changed
- 修改 Code 

##[0.5.25] - 2022-8-9
### Changed
- 修改 NPC 

##[0.5.24] - 2022-8-9
### Changed
- 修改 ClaimEquipmentReq 消息
- 修改 GetsCraftedEquipmentRes 消息


##[0.5.23] - 2022-8-8
### Changed
- 添加 UsedEquipmentGoodReq 消息

##[0.5.22] - 2022-8-8
### Changed
- 修改 SmeltingEquipmentReq 消息


##[0.5.21] - 2022-8-7
### Changed
- 修改 EquipmentInfo 消息
- 修改 PlayerEquipment 消息

##[0.5.18] - 2022-8-7
### Added
- 添加 RegainStrengthReq 消息

##[0.5.17] - 2022-8-4
### Changed
- 修改 FamilyHouse 消息

##[0.5.16] - 2022-8-4
### Added
- 添加 ClaimEquipmentReq 消息
- 添加 GetsCraftedEquipmentReq 消息



##[0.5.13] - 2022-8-4
### Changed
- 修改 ProduceEquipmentReq 消息


##[0.5.12] - 2022-8-4
### Changed
- 添加 GainAllRecipeReq 消息

##[0.5.9] - 2022-8-3
### Changed
- FamilyHouseInfo 消息


##[0.5.8] - 2022-8-3
### Changed
- 添加 Code 参数 FamilyBuildPosOverlap 

##[0.5.7] - 2022-8-2
### Added
- 添加 ClaimResourcesReq 消息
- 添加 SmeltingEquipmentReq 消息
- 添加 BuildingHouseMsgNotify 消息
- 添加 ProductionMsgNotify 消息
- 添加 BuildingHouseMsg 结构


##[0.5.6] - 2022-7-29
### Changed

- 修改 FamilyHouseInfo 结构
- 添加 ExtendForgeShop 结构
- 添加 ExtendResourceOutputs 结构
- 添加 ExtendSpecial 结构
- 添加 ExtendMainCity 结构


##[0.5.03] - 2022-7-28
### Changed
- 增加 GetsRecipeReq 消息
- 修改 GetsAvailablePlayerRecipeReq 消息


##[0.4.99] - 2022-7-27
### Changed
- 修改 Recipe 结构
- 修改 PlayerEquipment 结构

##[0.4.98] - 2022-7-27
### Changed
- 修改 PingReq 消息


##[0.4.97] - 2022-7-27
### Changed
- 修改 NpcPerson 结构

##[0.4.95] - 2022-7-27
### Changed
- 修改 GetsAvailableEquipmentInfoReq -->GetsAvailableRecipeRes
- 添加 GetsAvailablePlayerRecipeReq 消息

##[0.4.93] - 2022-7-26
### Changed
- 修改 FamilyHouseInfo 消息

##[0.4.91] - 2022-7-26
### Added
- 修改 Recipe 结构
- 添加 PlayerRecipe 结构 
- 修改 ProduceEquipmentReq 消息


##[0.4.90] - 2022-7-25
### Added
- 添加 ProduceEquipmentReq 消息


##[0.4.87] - 2022-7-25
### Changed
- 修改 Harm 结构



##[0.4.84] - 2022-7-24
### Added
- 添加 GetShoppersNpcDetailReq 消息
- 添加 RefusalToSellReq 消息
- 添加 GetsAvailableEquipmentReq 消息
- 添加 ToggleSellingEquipmentReq 消息
- 添加 GetsAvailableEquipmentInfoReq 消息
- 添加 GetsMakingEquipmentReq 消息
- 添加 MobileBuildingReq 消息 
- 添加 NpcAppearMessageNotify 消息通知


##[0.4.81] - 2022-7-22
### Added
- 添加 AppreciateReq 消息
- 添加 PriceReductionReq 消息
- 添加 GetsShoppersNpcReq 消息

##[0.4.80] - 2022-7-21
### Added
- 添加 CreateHomeReq 消息
- 添加 ConstructionHomeBuildingsReq 消息
- 添加 UpgradingBuildingsReq 消息
- 添加 GetBuildingReq 消息
- 添加 FamilyHouse 消息
- 添加 PlayerFamily 消息
- 添加 PlacePos 消息
- 添加  OfferForSaleReq 消息
- 添加 GetsFamilyHouseInfoReq 消息
- 添加 GetsFamilyHouseReq 消息
- 添加 AppreciateReq 消息
- 添加 PriceReductionReq 消息
- 添加 FamilyHouseInfo 消息

##[0.4.73] - 2022-7-20
### Changed
- 修改 LearningSkillsReq 消息 


##[0.4.72] - 2022-7-19
### Changed
- 修改 PosMonster 结构

##[0.4.71] - 2022-7-14
### Added
- 增添 GetsSkillEffectsReq 消息


##[0.4.68] - 2022-7-11
### Changed
- 删除 SimpleRoundReportRes 消息
- 修改 GetBattleDetailInfoRes 消息


##[0.4.68] - 2022-7-11
### Added
- 添加 GetBattleDetailDateReq 消息
- 修改 ReportMsg 消息


##[0.4.65] - 2022-7-1
### Added
- 修改 LearningAllSkillsTestReq 消息


##[0.4.64] - 2022-6-16
### Changed
- 修改 CombatTestReq 消息
- 修改 CombatTestRes 消息

##[0.4.62] - 2022-6-16
### Changed
- 修改 ReportMsg 结构


##[0.4.61] - 2022-6-8
### Changed
- 修改 QuickenParameter 消息
- 修改 StageNotify 消息

##[0.4.59] - 2022-6-7
### Changed
- 修改 KillMonsterNotify 消息

##[0.4.56] - 2022-6-6
### Changed
- 修改 VisiblePosNotify 消息


##[0.4.53] - 2022-6-4
### Changed
- 修改 landArmy 结构
- 修改 VisibleArmyNotify 消息
- 修改 VisibleCombatNotify 消息
- 修改 BattlePos 结构

##[0.4.52] - 2022-6-3
### Changed
- 修改 KillMonsterRes 消息
- 需改 KillMonsterNotify 消息


##[0.4.51] - 2022-6-3
### Added
- 添加 VisibleCombatNotify 消息



##[0.4.48] - 2022-6-3
### Changed
- 修改 VisibleArmyNotify 消息

##[0.4.47] - 2022-6-3
### Changed
- 修改 GetBattleDetailInfoReq 消息
- 修改 GetBattleDetailRes 消息
- 修改 KillMonsterNotify 消息

##[0.4.45] - 2022-6-2
### Changed
- 修改 KillMonsterRes 消息
- 添加 ConstructionAccelerationNotice 消息

##[0.4.43] - 2022-5-29
### Changed
- 修改 BuildNeedGoods 结构

##[0.4.42] - 2022-5-27
### Added
- 添加 GetsPosMonsterReq 消息


##[0.4.41] - 2022-5-27
### Added
- 添加 MonsterInfo 结构
- 修改 KillMonsterRes 消息
- 添加 KillMonsterNotify 消息

##[0.4.31] - 2022-5-26
### Changed
- 修改 StageNotify 消息


##[0.4.29] - 2022-5-25
### Changed
- 修改 KillMonsterReq 消息

##[0.4.27] - 2022-5-24
### Changed
- 修改 StageNotify 消息
- 修改 AdventureWarReport 结构

##[0.4.25] - 2022-5-23
### Changed
- 修改 GetsLandStatusRes 消息

##[0.4.23] - 2022-5-22
### Changed
- 修改 GetsMailRewardsRes 消息
- 修改 ReadMailRes 消息
- 修改 GetsAllMailRewardRes 消息

##[0.4.22] - 2022-5-20
### Changed
- 修改 AssistParameter 结构

##[0.4.21] - 2022-5-20
### Changed
- 修改 PublishPersonalCollectTaskReq 消息
- 添加  Code_TaskNotExist 


- 修改 TaskGuild 结构
- 修改 CollectParameter 结构

##[0.4.19] - 2022-5-19
### Changed
- 修改 AssistParameter 结构


##[0.4.15] - 2022-5-19
### Changed
- 修改 TaskGuild 结构 
- 修改 PublishGuildDefenseTaskRes 消息
- 修改 PublishGuildAttackTaskRes 消息
- 修改 PublishGuildKillMonsterTaskRes 消息
- 修改 PublishGuildKillEnemyTaskRes 消息
- 修改 PublishGuildAssistTaskRes 消息

##[0.4.10] - 2022-5-19
### Changed
- 修改 TaskGuild 结构
- 修改 PublishGuildCollectTaskRes 消息
- 修改 Collect 结构


##[0.4.6] - 2022-5-18
### Deleted
- 删除 AcceptTaskReq
- 删除 ReceiveRewardsReq 
- 删除 GetsPlayerTaskGuildReq
- 删除 GetsSystemTaskReq

##[0.4.5] - 2022-5-18
### Changed
- 修改 CollectParameter 结构

##[0.4.2] - 2022-5-18
### Added
- 修改 GetsMyTypeItemReq 消息
- 添加 GetsTypeItemReq 消息

##[0.3.99] - 2022-5-17
### Added
- 修改 ExecutionAssistTaskReq 消息

##[0.3.98] - 2022-5-17
### Changed
- 修改 TaskGuild 消息
- 修改 PlayerTaskGuild 消息

##[0.3.95] - 2022-5-17
### Added
- 添加 PublishGuildAssistTaskReq 消息
- 添加 TaskAssist 消息

##[0.3.94] - 2022-5-15
### Changed
- 添加 GetsArmyDetailsReq 消息
- 添加 ArmyDetails 消息

##[0.3.93] - 2022-5-15
### Changed
- 修改 Army 结构
- 修改 ClickNotify 消息

##[0.3.91] - 2022-5-8
### Changed
- 修改 LandArmy 消息



##[0.3.90] - 2022-5-6
### Changed
- 修改 GetsPlayerSkillRes 消息
- 修改 GetsLoadingSkillRes 消息
- 修改 GetsPlayerGoodRes 消息


##[0.3.88] - 2022-5-3
### Changed
- 修改 ConstructInfo 结构

##[0.3.87] - 2022-5-3
### Changed
- 修改 TakeThePlungeSwitchRes 消息
- 修改 GetAdventureTeamRes 消息



##[0.3.86] - 2022-5-3
### Changed
- 修改 GridRewards 结构

##[0.3.85] - 2022-5-3
### Changed
- 修改 TakeThePlungeSwitchRes 消息

##[0.3.84] - 2022-5-3
### Changed
- 修改 TakeThePlungeSwitchRes 消息
- 添加 AdventureRewards 结构
- 修改 StageNotify 消息

##[0.3.83] - 2022-5-2
### Changed
- 修改 TakeThePlungeSwitchRes 消息
- 添加 AdventureWarReport 结构


##[0.3.81] - 2022-4-27
### Changed
- 修改 AdventureTroop 结构
- 修改 MapData 结构

##[0.3.77] - 2022-4-27
### Changed
- 修改 GetAdventureTeamRes 消息



##[0.3.76] - 2022-4-27
### Added
- 修改  MapData 结构


##[0.3.75] - 2022-4-27
### Added
- 添加 GetAdventureTeamReq 
- 修改CreateAdventureTeamReq


##[0.3.69] - 2022-4-27
### Changed
- 修改 PublishCollectTaskReq -> PublishPersonalCollectTaskReq
- 修改 GetsPublishListReq -> GetsPersonalTaskReq 
- 修改 PublishPersonalAccelerateRecruitTaskReq ->PublishQuickenTaskReq
- 修改 SubmitItemsReq ->SubmitPersonalCollectTaskReq
- 修改 GetsPublishTaskGuildListReq -> GetsGuildTaskReq
- 修改 PublishGuildSoldierTaskReq ->PublishGuildKillEnemyTaskReq
- 修改 PublishGuildQuickenTaskReq -> PublishGuildAccelerateBuildTaskReq
- 修改 SubmitGuildItemsReq -> SubmitGuildCollectTaskReq
- 修改 GuildClickQuickenReq -> HelpGuildAccelerateBuildTaskReq
- 修改 GetSystemTaskReq -> GetsSystemTaskReq
- 修改 PublishGuildMonsterTaskReq -> PublishGuildKillMonsterTaskReq

- ##[0.3.68] - 2022-4-24
### Added
- 添加 RmbSoldierUpgradesReq 消息


##[0.3.66] - 2022-4-24
### Changed
- 添加 OneClickAccelerationReq 消息
- 修改 RmbRecruitTrainingGroundReq 消息


##[0.3.65] - 2022-4-24
### Changed
- 修改 TrainingGroundTask 结构
- 修改 PublishQuickenTaskReq 消息

##[0.3.63] - 2022-4-22
### Changed
- 修改 Person 结构

##[0.3.61] - 2022-4-22
### Changed
- 修改 RecruitSoldiersReq 消息


##[0.3.60] - 2022-4-22
### Changed
- 修改 PlayerGood 结构


##[0.3.59] - 2022-4-20
### Changed
- 删除 GetPersonUpgradeReq 消息
- 修改 Suitability 枚举


##[0.3.58] - 2022-4-19
### Added
- 添加  GetsPersonDetailReq 消息

##[0.3.57] - 2022-4-19
### Changed
- 修改  GetTypeItemReq 消息

##[0.3.54] - 2022-4-19
### Changed
- 修改 GetPersonUpgradeReq 消息
- 添加  GetTypeItemReq 消息

##[0.3.52] - 2022-4-18
### Changed
- 修改 RecruitTrainingGroundReq 消息

##[0.3.50] - 2022-4-18
### Changed
- 修改 AddQueueReq 消息

##[0.3.49] - 2022-4-18
### Changed
- 修改 ClickNotify 消息


##[0.3.48] - 2022-4-18
### Added
- 添加 AddQueueReq 消息
- 修改 ConscriptionArticleRes 消息


##[0.3.47] - 2022-4-18
### Changed
- 修改 GetsTrainingGroundReq 消息
- 修改 TrainingGround 结构

##[0.3.45] - 2022-4-16
### Added
- 添加 SearchPlayerRes 消息


##[0.3.43] - 2022-4-16
### Changed
- 修改  GuildSendEmailReq 消息
- 修改  PlayerSendEmailReq 消息

##[0.3.42] - 2022-4-16
### Added
- 添加  GuildSendEmailReq 消息
- 添加  PlayerSendEmailReq 消息


##[0.3.41] - 2022-4-16
### Changed
- 修改 ConstructInfo 结构
- 修改 EquipmentInfo 结构
- 修改 Good 结构  





##[0.3.32] - 2022-4-11
### Changed
- 修改 SoldierUpgradesReq 消息
- 修改 ConscriptionArticleRes 消息


##[0.3.29] - 2022-4-11
### Changed
- 修改 PublishQuickenTaskReq 消息 task_id
- 修改 TrainingParameter 结构 task_id

##[0.3.27] - 2022-4-11
### Added
- 添加 ConscriptionArticleReq 消息
- 添加 ArmyUpgradeReq 消息



##[0.3.26] - 2022-4-11
### Changed
- 修改 CreateArmyReq 消息
- 修改 ReviseArmyReq 消息
- 修改 AllocateTroopsReq 消息


##[0.3.24] - 2022-4-9
### Added
- 添加 AllocateTroopsReq 消息
- 添加 ReviseArmyReq 消息


##[0.3.22] - 2022-4-8
### Changed
-修改 GetLoginRewardReq 消息

##[0.3.21] - 2022-4-8
### Changed
- 添加 GetLoginRewardReq 消息
- 添加 GetsLoginRewardReq 消息
- 添加 GetLoginMsgReq 消息

##[0.3.18] - 2022-4-7
### Changed
- 添加 CreateAdventureTeamReq消息


##[0.3.15] - 2022-4-7
### Changed
- 删除 GetsRewardReq 消息


##[0.3.13] - 2022-4-6
### Changed
- 添加 GetsLoadingSkillReq 消息


##[0.3.12] - 2022-4-6
### Changed
- 修改 BasePlayer 结构
- 添加 GetsRewardReq 消息
- 修改 GetsPlayerSkillReq 消息


##[0.3.8] - 2022-4-6
### Changed
- 修改 TakeThePlungeSwitchReq 消息

##[0.3.7] - 2022-4-5
### Changed
- 修改 PlayerEquipment 模块
- 修改 PlayerGood 模块

##[0.3.5] - 2022-4-4
### Added
- 添加 Activity 模块


##[0.3.4] - 2022-4-4
### Changed
-修改 skill 结构 picture

##[0.3.3] - 2022-4-3
### Changed
-修改 Gift 结构


##[0.3.0] - 2022-4-3
### Changed
- 修改 GetsMailReq 消息
- 修改 GetsAllMailRewardReq 消息
- 修改 DeleteAllReadMailReq 消息


##[0.2.99] - 2022-4-3
### Changed
- 添加 Mail 结构 mail_type

##[0.2.97] - 2022-4-2
### Changed
- 修改 GetsCommodityReq 消息

##[0.2.94] - 2022-3-30
### Added
- 添加 ArmsUpgradeBookReq 消息
- 添加 TermRefreshReq 消息

##[0.2.93] - 2022-3-27
### Changed
- 修改 TaskPublish 结构
- 添加 TrainingParameter 结构


##[0.2.91] - 2022-3-27
### Changed
- 修改 PublishQuickenTaskReq 消息


##[0.2.90] - 2022-3-27
### Added
- 添加 GetTrainingGroundReq 消息
- 添加 RecruitTrainingGroundReq 消息
- 添加 RmbRecruitTrainingGroundReq 消息
- 添加 TrainingGround 结构  

##[0.2.84] - 2022-3-24
### Added
- 添加 GetSystemTaskReq 消息


##[0.2.82] - 2022-3-24
### Changed
- 修改 repeated 后面的变量为复数


##[0.2.80] - 2022-3-24
### Changed
- 修改 GetsPlayerGoodReq 消息
- 修改 GetsPlayerGoodRes 消息
- 修改 PlayerGoods 结构


##[0.2.79] - 2022-3-22
### Changed
- 修改 PublishGuildQuickenTaskReq 消息
- 修改 GuildClickQuickenRes 消息 去掉 reward_list

##[0.2.76] - 2022-3-22
### Added
- 添加 PublishGuildQuickenTaskReq 消息
- 添加 PublishGuildQuickenTaskRes 消息
- 添加 GuildClickQuickenReq 消息


##[0.2.73] - 2022-3-16
### Added
- 添加 GetsPublishListReq 消息
- 添加 GetsPublishTaskGuildListReq 消息
- 添加 GetsPlayerTaskGuildReq 消息
- 添加 SubmitGuildItemsReq 消息




##[0.2.67] - 2022-3-16
### Added
- 添加 PublishGuildCollectTaskReq 消息
- 添加 PublishGuildDefenseTaskReq 消息
- 添加 PublishGuildAttackTaskReq 消息
- 添加 PublishGuildMonsterTaskReq 消息
- 添加 PublishGuildSoldierTaskReq 消息

##[0.2.65] - 2022-3-16
### Added
- 添加 PublishGuildCollectTaskReq 消息
- 添加 PublishGuildBattleTaskReq 消息

##[0.2.63] - 2022-3-16
### Added
- 添加 SubmitItemsReq 消息
- 添加 ClickQuickenReq 消息


##[0.2.61] - 2022-3-15
### Added
- 添加 PublishCollectTaskReq 消息
- 添加 PublishQuickenTaskReq 消息
- 删除 PublishTaskReq 消息

##[0.2.60] - 2022-3-14
### Added
- 添加 PublishTaskReq 消息
- 添加 PublishTaskRes 消息
- 添加 GetsMyTaskReq 消息
- 添加 GetsMyTaskRes 消息
- 添加 AbandonTaskReq 消息
- 添加 AbandonTaskRes 消息

##[0.2.57] - 2022-3-13
### Changed
- Commodity 结构


##[0.2.55] - 2022-3-13
### Changed
- 修改 Commodity 结构
- 添加 Task 模块




##[0.2.54] - 2022-3-13
### Changed
- 修改 GetsGuildAppliesReq 消息 GetsGuildApplyReq 
- 修改 GetsGuildAppliesRes 消息 GetsGuildApplyRes
- 修改 GetsPlayerGoodsReq 消息 GetsPlayerGoodReq



##[0.2.53] - 2022-3-13
### Changed
- 修改 GetApplyGuildReq 消息 ApplyGuildReq 消息

##[0.2.52] - 2022-3-13
### Changed
- 修改 GetsTasksRes 消息
- 修改 gets_gift_req 消息

##[0.2.51] - 2022-3-13
### Changed
- 修改 GetsCommodityReq 消息 
- 修改 GetsGiftReq 消息
- 修改 GetsCommodityRes 消息
- 修改 GetsGiftRes 消息
- 修改 GetsGuildMemberReq 消息
- 修改 GetApplyGuildReq 消息
- 修改 GetsGuildReq 消息
- 修改 GetsGuildMemberRes 消息
- 修改 GetApplyGuildRes 消息
- 修改 GetsGuildAppliesRes 消息
- 修改 GetsGuildPersonsRes 消息
- 修改 GetsGuildRes 消息
- 修改 GetBattleDetailReq 消息
- 修改 GetBattleDetailRes 消息

- 修改 BattleTask 消息


##[0.2.50] - 2022-3-10
### Changed
- 修改 ReceiveRewardsRes 结构 



##[0.2.49] - 2022-3-9
### Changed
- 修改 PCollect 结构 condition_type

##[0.2.47] - 2022-3-9
### Changed
- 修改 Task 结构 reward_list
- 添加 RewardList 结构

##[0.2.46] - 2022-3-9
### Changed 
- 修改 Collect 消息 condition_type
  

##[0.2.45] - 2022-3-8
### Added
- 添加 VisibleNotify 消息 


##[0.2.43] - 2022-3-8
### Changed
- 修改 MapData 结构 race_id


##[0.2.42] - 2022-3-8
### Added
- 添加 GetsDrawPersonReq 消息
- 添加 GetsDrawPersonRes 消息
- 修改 DrawSkillsReq 消息 money_type

##[0.2.41] - 2022-3-3
### Added
- 添加 GetsAssemblyListReq 消息
- 添加 GetsAssemblyListRes 消息
- 添加 JoinAssemblyReq 消息
- 添加 JoinAssemblyRes 消息
- 添加 MassInfo 结构
- 添加 ArmyList 结构



##[0.2.35] - 2022-3-2
### Changed
- 修改 ArmyStatus 结构 massing


##[0.2.30] - 2022-3-2
### Changed
- 修改 BuildMsg 结构 level

##[0.2.28] - 2022-3-1
### Added
- 添加 InitiateAssemblyReq 消息
- 添加 InitiateAssemblyRes 消息

##[0.2.27] - 2022-3-1
### Changed
- 修改 Guild 字段 rider_level saber_level lancer_level archer_level

##[0.2.24] - 2022-2-28
### Changed
- 添加 UpgradeArmsReq 消息 army_id


##[0.2.24] - 2022-2-28
### Added
- 添加 UpgradeArmsRes 消息 
- 添加 UpgradeArmsReq 消息

##[0.2.23] - 2022-2-28
### Changed
- 修改 army 结构 增加 army_attribute army_level
- 添加 CreateArmyReq 消息 增加army_level 去掉is_king 

##[0.2.19] - 2022-2-20
### Changed
- 修改 GetDischargeRes 消息 code
- 修改 GetCommodityRes 消息 code
- 修改 GetGiftRes 消息 code

##[0.2.18] - 2022-2-20
### Changed
- 修改 KingCreateArmyReq 消息
- 修改 KingCreateArmyRes 消息

##[0.2.13] - 2022-2-16
### Changed
- 修改 CreateSignRes 消息
- 修改 DeleteSignRes 消息


##[0.2.13] - 2022-2-16
### Changed
- 修改 GetsPersonReq 消息 is_king
- 添加 DrawPersonReq 消息 is_king
- 添加 LoadPersonSkillReq 消息 is_king
- 添加 UnLoadPersonSkillReq 消息 is_king
- 添加 GetsArmyReq 消息 is_king
- 添加 GetArmyReq 消息 is_king
- 添加 KingSoldierReq 消息 
- 添加 KingSoldierRes 消息 

##[0.2.12] - 2022-2-15
### Changed
- 添加 CreateSignReq 消息
- 添加 CreateSignRes 消息
- 添加 DeleteSignReq 消息
- 添加 DeleteSignRes 消息

##[0.2.8] - 2022-2-13
### Changed
- 增添 Construct  结构 name 字段 


##[0.2.7] - 2022-2-13
### Changed
- 增添 GetsConstructInfoReq  消息 race_id


##[0.2.3] - 2022-2-11
### Changed
- 增添 GetPlayerRes  消息 role
 

##[0.2.3] - 2022-2-2
### Changed
- 增添 GetMyGuildRes  消息 


##[0.1.200] - 2022-2-2
### Changed
- 增添 SetAuthorityRes  消息
- 增添 DismissMemberRes  消息



##[0.1.199] - 2022-2-2
### Added
- 增添 GetMyGuildReq  消息
- 增添 GetMyGuildRes  消息



##[0.1.198] - 2022-1-31
### Changed
- 修改 GetPersonRes tag person

##[0.1.195] - 2022-1-18
### Changed
- 修改 WarReport 消息


##[0.1.193] - 2022-1-16
### Changed
- 修改 SimpleRoundReportRes 消息 
- 修改 DrawSkillsRes 消息的 skill 字段å



##[0.1.186] - 2022-1-15
### Changed
- 增添 SimpleRoundReportRes 消息 pos字段

##[0.1.183] - 2022-1-14
### Changed
- 增添 VisibleArmyNotify 消息 status,pos



##[0.1.181] - 2022-1-14
### Changed
- 增添 OccupyPos 消息 map_data,guild_id,guild_name


##[0.1.175] - 2022-1-14
### Added
- 增添 GetsMapDataReq 消息


##[0.1.174] - 2022-1-13
### Added
- 增添 Guild 的pos字段





##[0.1.173] - 2022-1-12
### Added
- 增添 GetsMapResourcesReq
- 增添 GetsMapResourcesRes



所有更新内容都将会在此文件中记录。
##[0.1.169] - 2022-1-11
### Changed
- 修改 GetMapPosesReq 改为GetsMapPosReq
- 修改 GetMapPathsReq 改为GetsMapPathReq

##[0.1.168] - 2022-1-11
### Changed
- 修改 SimpleRoundReport 的 army 字段


##[0.1.167] - 2022-1-11
### Changed 
- 修改 SimpleRoundReport 的 person 字段



##[0.1.164] - 2022-1-9
### Added
- 新增 GetsLandStatusReq 消息
- 新增 GetsLandStatusRes 消息



##[0.1.163] - 2022-1-9
### Changed
- 修改 CustomPerson 消息 arms
- 修改 Destroy 消息
- 修改 OccupyPos 消息




##[0.1.159] - 2022-1-9
### Changed
- 修改 CreateArmyReq 消息 person2_id,person3_id



##[0.1.157] - 2022-1-9

### Added
- 增加 CustomPerson 消息的 



##[0.1.154] - 2022-1-7

### Changed
- 修改 Person 消息的 load_skills


##[0.1.153] - 2022-1-7

### Changed
- 修改 Notice 消息的 



##[0.1.149] - 2022-1-7

### Changed
- 修改 SimpleRoundReportRes 消息的
- 修改 GetsPersonRes 消息 code
- 修改 GetPersonRes 消息 code
- 修改 GetPersonUpgradeRes 消息 code
- 修改 GenealogyRes 消息 code


##[0.1.145] - 2022-1-4

### Changed
- 修改 Chat 消息的



##[0.1.141] - 2022-1-4
### Added
- 增加 ApplyCancelRelationReq 消息


##[0.1.138] - 2022-1-3
### Changed
- 增加 ArmyStatus 状态 Occupy


##[0.1.137] - 2022-1-3
### Changed
- 修改 GuildRelation 


##[0.1.136] - 2022-1-3
### Changed
- 修改 BasePlayer 消息  avatar_address


##[0.1.133] - 2022-1-3

### Added
- 添加 GetsWarReportRes 消息


##[0.1.132] - 2022-1-2

### Changed
- 删除 DeclareWarRes 消息  GuildCombat




##[0.1.131] - 2022-1-2

### Added
- 添加 SimpleRoundReportRes 消息 SimpleRoundReport





##[0.1.130] - 2022-12-30

### Changed

- 添加 GetsArmyRes 消息 的 Code
- 添加 GetArmyRes 消息 的 Code
- 添加 GuildMemberRes 消息 的 Code
- 添加 GuildAppliesRes 消息 的 Code
- 添加 ChatAllRes 消息 的 Code
- 添加 GuildChatRes 消息 的 Code
- 添加 ChatPlayerRes 消息 的 Code
- 添加 GetsPlayerGoodsRes 消息 的 Code
- 添加 GetsPlayerEquipmentRes 消息 Code
- 添加 RecycleRes 消息 Code
- 添加 GetsPlayerSkillRes 消息 Code
- 添加 GetsSkillRes 消息 Code
- 添加 GetEquipmentInfoRes 消息 Code




## [0.1.8] - 2022-11-15

### Added

- 添加 CHANGELOG.md 文件。

## [0.1.12] - 2022-11-15

### Added

- 增加 PlayerGoods 的 name。

### Removed

- 去掉 GuildChatReq 的 guild_id。

## [0.1.13] - 2022-11-16

### Added

- 增加 GetsPlayerSkillReq （获取契约列表）相关 protocol。
- 增加 DrawSkillsReq （抽取契约）相关 protocol。

## [0.1.14] - 2022-11-16

### Added

- 增加 CreateConstruct。
- 增加 UpgradeConstruct。
- 增加 RepairConstruct。
- 增加 DemolishConstruct。
- 增加 Construct。

## [0.1.15] - 2022-11-16

### Added

- 增加 UpgradePath。
- 增加 MapPath。

## [0.1.16] - 2022-11-16

### Added

- 增加 MapPos。
- 增加 GetMapPoses。
- 增加 GetMapPaths。

## [0.1.17] - 2022-11-16

### Changed

- 修改 MapPos、MapPath 字段。

## [0.1.18] - 2022-11-16

### Added

- 添加 ConstructInfo 消息。

## [0.1.19] - 2022-11-16

### Fixed

- 修复 ConstructInfo 类型错误。

## [0.1.20] - 2022-11-19

### Added

- 添加 MapPathStatusInit 消息。

## [0.1.21] - 2022-11-21

### Added

- 添加 NeedLandform 消息。
- 添加 NeedConstruct 消息。
- 添加 Resources 消息。
- 添加 Effect 消息。
- 添加 Equipment 消息。
- 添加 EquipmentPos 消息。

## [0.1.22] - 2022-11-22

### Added

- 添加 RecipePremise 消息。

### Changed

- 修改 Resources 为 SomeResource 消息。
- 修改 Effect 为 AttributeEffect 消息。

## [0.1.22] - 2022-11-25


### Changed
- 添加 PurchaseEquipmentReq 消息
- 添加 GetsPlayerEquipmentReq 消息
- 添加 RecycleEquipmentReq 消息
- 添加 UseSupplyItemReq 消息
- 添加 UsePotionItemReq 消息




## [0.1.30] - 2022-11-25

### Changed

- 修改 Path 消息。

## [0.1.31] - 2022-11-25

### Changed

- 修改 Path 消息。

## [0.1.32] - 2022-11-25

### Added

- 添加 MapPosStatusInit 消息。

### Changed

- 修改 MapPathStatusInit 消息。

## [0.1.34] - 2022-11-25

### Changed

- 修改 MapPathStatusInit => MapPathStatus 消息。
- 修改 MapPosStatusInit => MapPosStatus 消息。

## [0.1.41] - 2022-11-26

### Added

- 添加错误码。

## [0.1.42] - 2022-11-26

### Added

- 添加错误码。


## [0.1.47] - 2022-12-6

### Added
- 新增 Person  => arms_suitability 字段。
- 新增 Person  => properties       字段。
- 新增 Person  => words            字段。


## [0.1.91] - 2022-12-19

### Added
- 新增 Land  =>  gets_construct 消息。



