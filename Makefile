killO:
	protoc \
	--go_out=. \
	--gorm_out=. \
	--go-grpc_out=. \
	-I . \
	-I ./proto/lib \
	proto/*.proto
	protoc-go-inject-tag -input="*.pb.go"

tag:
	echo v0.2.$(VER) > README.md
	git add *
	git commit -m "proto update"
	git tag v0.2.$(VER)
	git push origin v0.2.$(VER)

init:

	go get github.com/golang/protobuf/protoc-gen-go
	go install github.com/golang/protobuf/protoc-gen-go
	go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go get github.com/infobloxopen/protoc-gen-gorm
	go install github.com/infobloxopen/protoc-gen-gorm

# USAGE: make tag VER=6