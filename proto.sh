#!/bin/zsh

protoc \
--go_out=. \
--gorm_out=. \
--go-grpc_out=. \
-I . \
-I ./proto/lib \
proto/*.proto
protoc-go-inject-tag -input="*.pb.go"